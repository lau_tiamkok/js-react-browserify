var React = require('react');
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');

var Hello = React.createClass({
    render: function() {
        return <h1>Hello {this.props.name}</h1>;
    }
});

// ReactDOM.render(<Hello name="World" />, document.getElementById('example'));

// // renderedToString:
// var html = ReactDOMServer.renderToString(React.createElement(Hello, {name: 'Kok!!!'}));
// document.getElementById('example').innerHTML = html;

// // Or:
// var ProductApp = React.createFactory(Hello);
// var html = ReactDOMServer.renderToString(ProductApp({name: 'Kok'}));
// document.getElementById('example').innerHTML = html;

// renderToStaticMarkup:
var html = ReactDOMServer.renderToStaticMarkup(React.createElement(Hello, {name: 'Kok!!!'}));
document.getElementById('example').innerHTML = html;

// // Or:
// var ProductApp = React.createFactory(Hello);
// var html = ReactDOMServer.renderToStaticMarkup(ProductApp({name: 'Kok!'}));
// document.getElementById('example').innerHTML = html;
